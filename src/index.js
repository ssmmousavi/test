// load neccessary data in customer page
const customerPageLoad = () => {
  console.log('loaded');
  loadProduct()
  console.log(localStorage.getItem('products'))
  selectName()
}

// show product options for ordering one
const loadProduct = () => {
  console.log('loadProduct');

  const products = JSON.parse(localStorage.getItem('products'))
  let options = []
  Object.keys(products).map(product => {
    options.push(products[product].name)

  })
  const select = document.getElementById("productName");
  options.map(option => {
    const newOption = document.createElement("option");
    newOption.text = `${option}`;
    select.add(newOption);

  })
}

// show th eavaileble product left
const selectName = async () => {
  const chosenProduct = document.getElementById("productName").value
  const products = JSON.parse(localStorage.getItem('products'))
  document.getElementById('availebleProduct').value = products[chosenProduct].availebleProduct
  document.getElementById("varieantNumber").max = products[chosenProduct].availebleProduct
}

// submit btn for buying a car
const buyCar = () => {
  const name = document.getElementById('productName').value
  const varieantColor = document.getElementById('varieantColor').value
  const varieantYear = document.getElementById('varieantYear').value
  const varieantNumber = document.getElementById('varieantNumber').value
  const availebleProduct = document.getElementById('availebleProduct').value

  varieant(name,
    varieantColor,
    varieantYear,
    varieantNumber,
    availebleProduct
  )
}

// create an order from product object
const varieant = async (name, color, year, number, availeble) => {
  if (number > availeble) {
    document.getElementById('varieantNumber').style.background = '#e05353'
  } else {
    document.getElementById('varieantNumber').style.background = 'white'
    newOrder = new CreateNewProduct(name)
    newOrder.color = color
    newOrder.year = year
    newOrder.number = number


    if (localStorage.getItem('orders')) {
      const orders = JSON.parse(localStorage.getItem('orders'))
      orders.push(newOrder)
      localStorage.setItem('orders', JSON.stringify(orders))

      const products = JSON.parse(localStorage.getItem('products'))
      const numberLeft = parseInt(availeble) - parseInt(number)
      products[name].availebleProduct = toString(numberLeft)
      localStorage.setItem('products', JSON.stringify(products))

      document.getElementById('availebleProduct').value = numberLeft


    } else {
      localStorage.setItem('orders', JSON.stringify([newOrder]))
      const products = JSON.parse(localStorage.getItem('products'))
      const numberLeft = parseInt(availeble) - parseInt(number)
      products[name].availebleProduct = toString(numberLeft)
      localStorage.setItem('products', JSON.stringify(products))

      document.getElementById('availebleProduct').value = numberLeft

    }
  }

}

// constructor object for a product
function CreateNewProduct(name, silander = null, brand = null, availebleProduct = null) {
  this.name = name
  silander && (this.silander = silander)
  brand && (this.brand = brand)
  availebleProduct && (this.availebleProduct = availebleProduct)
}

// handeling submit btn in create a product form
const submitProduct = () => {
  // get value of every input
  const name = document.getElementById('productName').value
  const productSilanderNumber = document.getElementById('productSilanderNumber').value
  const availebleProduct = document.getElementById('availebleProduct').value
  const brand = document.getElementById('productBrand').value

  // create a new product and save it in localStorage

  // const newProduct = JSON.stringify(createNewProduct(name, productSilanderNumber, brand, availebleProduct))
  let data = {}
  // localStorage.setItem(`${name}`, data)
  if (localStorage.getItem('products')) {
    const products = JSON.parse(localStorage.getItem('products'))
    products[name] = new CreateNewProduct(name, productSilanderNumber, brand, availebleProduct)
    localStorage.setItem('products', JSON.stringify(products))
  } else {
    data[name] = new CreateNewProduct(name, productSilanderNumber, brand, availebleProduct)
    localStorage.setItem('products', JSON.stringify(data))
  }

  // clean inputs
  document.getElementById('productName').value = ''
  document.getElementById('productSilanderNumber').value = ''
  document.getElementById('productBrand').value = ''
  document.getElementById('availebleProduct').value = 0

}